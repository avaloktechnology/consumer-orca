# orca.rb 

A command-line tool for interacting with/manipulating a Jira Server instance.

Why "Orca?"

The ORCA is a device used to replicate the bioacoustics used by Titans,
  effectively communicating with them as well as controlling them to some extent.

Don't look at me like that- it's no weirder than Jira being named after Godzilla.
It's true, look it up.

## What does it do, Jim?
```
irb(main):001:0> require "./generic_orca.rb"
=> true
irb(main):002:0> j = Orca.new
=> #<Orca:0x0000564b3a1603d8>
irb(main):004:0> puts j.help
{
  "addComponent": {
    "method": "addComponent(slug,projectID,component)",
    "notes": "Adds a component to the target project"
  },
  "addGroups": {
    "method": "addGroups(grouplist)",
    "notes": "Adds a list of groups to Jira"
  },
  "addUsersToGroup": {
    "method": "addUsersToGroup(group,userlist)",
    "notes": "Adds a list of users to Jira."
  },
  "addWatchers": {
    "method": "addWatchers(issue,userlist)",
    "notes": "Adds a list of Watchers to the target issue."
  },
  "allGroups": {
    "method": "allGroups()",
    "notes": "Returns a list of all groups in Jira"
  },
  "gaiaCreateProject": {
    "method": "gaiaCreateProject(templateName,newProjectName,newProjectKey,projectLead,projectType)",
    "notes": "Creates a project using the Gaia for Jira plugin"
  },
  "getACL": {
    "method": "getACL(issue,permission)",
    "notes": "Retrieves an Access Control List (ACL) for the selected issue.  The ACL consists of ALL users with the ability to view the issue based on its current state."
  },
  "getGroup": {
    "methods": "getGroup(group)",
    "notes": "#Retrieves a Group object from Jira"
  },
  "getGroupMembers": {
    "method": "getGroupMembers(grouplist)",
    "notes": "Retrieves a list of members in the target group"
  },
  "listWatchers": {
    "method": "listWatchers(issue)",
    "notes": "List the watchers of the target issue"
  },
  "mapGroupToRole": {
    "method": "mapGroupsToRole(slug,roleID,grouplist)",
    "notes": "Associates a group with a given project role.  Custom aliases for roleID can be easily defined within the method."
  },
  "removeGroups": {
    "method": "removeGroups(grouplist)",
    "notes": "Remove a Group from Jira"
  },
  "removeWatchers": {
    "method": "removeWatchers(issue,userlist)",
    "notes": "Remove a list of Watchers from a given issue."
  },
  "valx": {
    "method": "valx(data,match)",
    "notes": "Utility method.  Extracts the <match>ing key from the returned data."
  },
  "vomit": {
    "method": "vomit()",
    "notes": "Returns a list of all the methods available to Orca."
  },
  "help": {
    "method": "help()",
    "new": "Returns this message."
  }
}
```
## Examples

jira.getACL("ABC-1","BROWSE")

jira.addUsersToGroup("x-users",["user_a","user_b"])

puts jira.getGroupMembers(["my","several","groups"])

jira.removeWatchers("ABC-2",["user_c"])

puts jira.allGroups

jira.listWatchers("ABC-3")

jira.removeGroups(["test-1","test-2"])

jira.addGroups(["group-1","group-2","group-3"])

